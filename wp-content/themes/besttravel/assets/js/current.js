'use strict';

jQuery(document).ready(function($){
	$('.current-menu-item a').addClass('active');
	$('#menu-item-142 > a').removeAttr('href');
	$('.parallax').parallax("50%", -0.5);
    
    /*
    checked
	*/
    
     function changeState(el) {
        if (el.readOnly) el.checked=el.readOnly=false;
        else if (!el.checked) el.readOnly=el.indeterminate=true;
    }

	/*
    bootstrap
	*/

	$(function () {
	  	$('[data-toggle="tooltip"]').tooltip();
	})
    
    $("input.val_insurant").TouchSpin({
        min: 1,
        max: 10
    });
    
    /*
	BUTTON CALL 
	*/
    
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 10,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//grab the "back to top" link
		$button_call = $('.cd-button-call');

	//hide or show the "back to top" link
	$(window).scroll(function(){
		( $(this).scrollTop() > offset ) ? $button_call.addClass('cd-is-visible') : $button_call.removeClass('cd-is-visible cd-fade-out');
		if( $(this).scrollTop() > offset_opacity ) { 
			$button_call.addClass('cd-fade-out');
		}
	});

	/*
	phone input mask
	*/

	$(".phone-inp").mask("999-9999999");

	$(".phone-inp").on("blur", function() {
	    var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );
	    
	    if( last.length == 3 ) {
	        var move = $(this).val().substr( $(this).val().indexOf("-") - 1, 1 );
	        var lastfour = move + last;
	        
	        var first = $(this).val().substr( 0, 9 );
	        
	        $(this).val( first + '-' + lastfour );
	    }
	});
	
});