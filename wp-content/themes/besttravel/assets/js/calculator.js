'use strict';

jQuery(document).ready(function($){
    /*
	datepicker
	*/
    $( function() {	
        var dateFormat = "dd.mm.yy",
            to = $( "#to-date" ).datepicker({
				minDate: 0,
				//changeMonth: true,
                dateFormat: dateFormat,
                numberOfMonths: 1
            })
            .on( "change", function() {
                from.datepicker( "option", "minDate", getDate( this ) );				
				
                // var date_step1 = to.datepicker('getDate', '+1d'); 
                var date_max = to.datepicker('getDate', '+180d'); 

                // date_step1.setDate(date_step1.getDate(this)+1); 
                date_max.setDate(date_max.getDate(this)+180); 

				// from.datepicker( "option", "minDate", date_step1 );
                from.datepicker( "option", "maxDate", date_max );				
				
				getDiffDay(to.val(), from.val());
				$('#from-date').removeAttr('disabled');
            }),
            from = $( "#from-date" )
            .datepicker({
				//changeMonth: true,
                dateFormat: dateFormat,
                numberOfMonths: 1
            })
            .on( "change", function() {
                  to.datepicker( "option", "maxDate", getDate( this ) );
				  getDiffDay(to.val(), from.val());
            });

        function getDate( element ) {
            var date;
            try {
                date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
                date = null;
            }

            return date;
        }
    });
	
	// Calculator 
	var dp_options = { defaultDate: '-37y',  dateFormat: "dd.mm.yy", changeYear: true, changeMonth: true, maxDate: 0, yearRange: "c-100:c" };
	$('input[name="birthday[]"]').datepicker(dp_options);

	var i = $('.hasBirthday').size() + 1;
	$('.bootstrap-touchspin-up').on('click touchstart', function() {
		var translate_parse = JSON.parse(translate);
		var html = '<div class="col-sm-6 hasBirthday">';
			html +=	'<div class="form-group calendar-group">';
			html +=	'<label for="birthday">' + translate_parse.text_calculate_8 + ':</label>'
			html +=	'<input type="text" name="birthday[]" class="form-control" readonly>'
			html += '</div>';
			html += '</div>';
			
		if(i <= 10) {
			$(html).fadeIn('slow').appendTo('.form-content .row.row-wrap');
			$('input[name="birthday[]"]').datepicker(dp_options);			
			i++;
		}
	});
	
	$('.bootstrap-touchspin-down').on('click touchstart', function() {
		console.log(i);
		if(i > 2) {
			$('.hasBirthday:last').remove();
			i--; 
		}	
	});	
	
	$('.bootstrap-touchspin-reset').on('click touchstart', function() {
		while(i > 2) {
			$('.hasBirthday:last').remove();
			i--;
		}
		$('span.diff-day').text(0);
		$('[name="number_of_insured"]').val(1);
		$('[name="birthday[]"], [name="to_date"], [name="from_date"]').datepicker('setDate', null);
	});	
	
	
	$('.step-1-next').on('click', function() {			
		var from = $('#from-date'),
			to = $('#to-date'),
			birthday = $('[name="birthday[]"]');
			
		if(!from.val()) {
			from.addClass('wpcf7-not-valid');
		} else {
			from.removeClass('wpcf7-not-valid');
		}		

		if(!to.val()) {
			to.addClass('wpcf7-not-valid');
		} else {
			to.removeClass('wpcf7-not-valid');
		}			
			
		if(!birthday.val()) {
			birthday.addClass('wpcf7-not-valid');
		} else {
			birthday.removeClass('wpcf7-not-valid');
		}				
			
		if(!from.val() || !to.val() || !birthday.val()) {
			$('.step-1 .hasErrorDisplayNone').show();
			return false;
		}

		$('.step-1').hide();
		$('.step-2').fadeIn('slow');	
	});
	
	$('[name="question"]').on('click', function() {
		if($(this).val() == 1) {
			$('.step-2-form').show();
			$('.step-3-btn, .hasErrorDisplayNone').hide();	
		} else {
			$('.step-2-form, .hasErrorDisplayNone').hide();
			$('.step-3-btn').show();
		}
	});
	
	$('.step-2-btn').on('click', function() {
		var firstname = $('.step-2 [name="firstname"]'),
			phone = $('.step-2 [name="phone"]'),
			confirm = $('.step-2 [name="confirm"]');
			
		if(!firstname.val()) {
			firstname.addClass('wpcf7-not-valid');
		} else {
			firstname.removeClass('wpcf7-not-valid');
		}	

		if(!phone.val()) {
			phone.addClass('wpcf7-not-valid');
		} else {
			phone.removeClass('wpcf7-not-valid');
		}			
			
		if(!firstname.val() || !phone.val() || !confirm.is(':checked')) {
			$('.step-2 .hasErrorDisplayNone').show();
			return false;
		}		
	});	

	$('.step-3-btn').on('click', function() {
		var confirm = $('.step-2 [name="confirm"]');
			
		if(!confirm.is(':checked')) {
			$('.step-2 .hasErrorDisplayNone').show();
			return false;
		}		
	});	
	
});

function getDiffDay(to, from) {
	var to = to + ' 00:00:00',
		from = from + ' 23:59:59',
		to_timestamp = moment(to, "DD.MM.YYYY HH:mm:ss").format('X'),
		from_timestamp = moment(from, "DD.MM.YYYY HH:mm:ss").format('X'); 

	var timeDiff = parseInt(from_timestamp) - parseInt(to_timestamp);
	var diffDays = Math.round(timeDiff / 86400);

	if(!isNaN(diffDays)) {
		var result = $('.diff-day').text(diffDays);
	} else {
		var result = 0;
	}
		
	return result;
}