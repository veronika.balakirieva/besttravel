/*!
 * Button visually impaired v1.0.5
 */
jQuery(document).ready(function($) {
    /* Активирует постоянную работу плагина */
    //$('.bvi-panel-open').bvi('Active');

    /* Пользовательские настройки */
    $('.bvi-panel-open').bvi('Init',{"BviPanelBg":"white","BviPanelFontSize":"11","BviPanelLetterSpacing":"normal","BviPanelCloseText": "","BviPanelLineHeight":"normal","BviPanelImg":"1","BviPanelImgXY":"1","BviPanelReload":"0","BviCloseClassAndId":".hide-screen-fixed","BviFixPanel":"0","BviPlay":"0"});

    /* Настройки по умолчанию */
    //$('.bvi-panel-open').bvi('Init');
});