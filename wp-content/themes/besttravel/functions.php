<?php
/**
 * @link http://tanasov.com
 * @author Tanasov Andrey <tanasov.dev@gmail.com>
 */

if(!function_exists('besttravel_setup')) {
	function besttravel_setup() {
		// Add Post Thumbnails Support and Related Image Sizes
		add_theme_support('post-thumbnails');
		add_image_size('besttravel-price-compare', 170, 100, true);
		add_image_size('besttravel-useful-for-tourists', 225, 120, true);
		add_image_size('besttravel-useful-for-tourists-category', 370, 290, true);
		add_image_size('besttravel-articles-category', 470, 279, true);
		add_image_size('besttravel-articles-category-block', 270, 270, true);
		add_image_size('besttravel-usefull-category-block', 370, 290, true);
		//add_image_size('besttravel-full', 550, 550, true);
			
		// Make theme available for translation, translations can be filed in the /languages/ directory	
		load_theme_textdomain('besttravel', get_template_directory() . '/languages');
		
		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */		
		add_theme_support('title-tag');
		
		// This theme uses post format support.
		add_theme_support('post-formats', array(
			'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
		));		
		
		// Add default posts and comments RSS feed links to head
		add_theme_support('automatic-feed-links');		
		
		// Remove tag wp_gead()		
		remove_action('wp_head', 'feed_links_extra', 3);
		remove_action('wp_head', 'feed_links', 2);
		remove_action('wp_head', 'rsd_link');
		remove_action('wp_head', 'wlwmanifest_link' );
		remove_action('wp_head', 'index_rel_link' );
		remove_action('wp_head', 'parent_post_rel_link', 10, 0);
		remove_action('wp_head', 'start_post_rel_link', 10, 0);
		remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
		remove_action('wp_head', 'wp_generator'); 		
		remove_action('wp_head', 'print_emoji_detection_script', 7);
		remove_action('wp_print_styles', 'print_emoji_styles');
		remove_action('wp_head', 'rest_output_link_wp_head');
		remove_action('wp_head','qtranxf_wp_head_meta_generator');		
	}
	add_action('after_setup_theme', 'besttravel_setup');
}

// Include CSS
function theme_css() {
	wp_enqueue_style('bootstrap-styles', esc_url(get_template_directory_uri()).'/assets/bootstrap/css/bootstrap' . l_rtl() . '.min.css', false, '0.1', 'all');
	wp_enqueue_style('bootstrap-offcanvas-styles', esc_url(get_template_directory_uri()).'/assets/bootstrap/css/bootstrap.offcanvas.css', false, '0.1', 'all');
	wp_enqueue_style('global-styles', esc_url(get_template_directory_uri()).'/assets/css/style.css', false, '0.1', 'all');
	wp_enqueue_style('jquery-ui-styles', esc_url(get_template_directory_uri()).'/assets/css/jquery-ui.css', false, '0.1', 'all');
	wp_enqueue_style('bvi-styles', esc_url(get_template_directory_uri()).'/assets/css/bvi.css', false, '0.1', 'all');
	wp_enqueue_style('custom-styles', esc_url(get_template_directory_uri()).'/assets/css/custom.css', false, '0.1', 'all');
}
add_action('wp_enqueue_scripts', 'theme_css');

// Include JS
function theme_js() {
	wp_enqueue_script('jquery-js', esc_url(get_template_directory_uri()) . '/assets/js/jquery.js', false, '0.1', true);
	wp_enqueue_script('responsivevoice-js', esc_url(get_template_directory_uri()) . '/assets/js/responsivevoice.js', false, '0.1', true);	
	wp_enqueue_script('bvi-lang', esc_url(get_template_directory_uri()) . '/assets/js/bvi-lang' . l_rtl() . '.js', false, '0.1', true);
	wp_enqueue_script('bvi-init-panel-js', esc_url(get_template_directory_uri()) . '/assets/js/bvi-init-panel.js', false, '0.1', true);
	wp_enqueue_script('bvi-js', esc_url(get_template_directory_uri()) . '/assets/js/bvi.js', false, '0.1', true);
	wp_enqueue_script('cookie-js', esc_url(get_template_directory_uri()) . '/assets/js/js.cookie.js', false, '0.1', true);
	wp_enqueue_script('bootstrap-js', esc_url(get_template_directory_uri()) . '/assets/bootstrap/js/bootstrap' . l_rtl() . '.min.js', false, '0.1', true);
	if(is_front_page()) {
		wp_enqueue_script('jquery-ui-js', esc_url(get_template_directory_uri()) . '/assets/js/jquery-ui/jquery-ui.js', false, '0.1', true);
		wp_enqueue_script('jquery-ui-datepicker-js', esc_url(get_template_directory_uri()) . '/assets/js/jquery-ui/datepicker-ru' . l_rtl() . '.js', false, '0.1', true);		
	}
	wp_enqueue_script('plugin-js', esc_url(get_template_directory_uri()) . '/assets/js/plugin.js', false, '0.1', true);
	wp_enqueue_script('moment-js', esc_url(get_template_directory_uri()) . '/assets/js/moment.min.js', false, '0.1', true);
	wp_enqueue_script('current-js', esc_url(get_template_directory_uri()) . '/assets/js/current.js', false, '0.1', true);
	if(is_front_page()) {
		wp_enqueue_script('calculator-js', esc_url(get_template_directory_uri()) . '/assets/js/calculator.js', false, '0.1', true);
	}
}
add_action('wp_enqueue_scripts', 'theme_js');

// This theme uses wp_nav_menu() in one location.
function register_nav_menus_on_init() {
	register_nav_menus(array(
		'menu-header' => __('Menu header', 'besttravel'),
		'menu-footer' => __('Menu footer', 'besttravel'),
	));
}
add_action('init', 'register_nav_menus_on_init');

#require_once(get_template_directory() . '/includes/startup.php');

// Include Custom Options for This Theme
//require_once(get_template_directory() . '/admin/theme-options.php');

// Add custom login logo
function LoginLogo() {
	echo '<style>#login h1 a { height: 44px; width: 138px; background: url("' . get_bloginfo('template_directory') . '/assets/images/logo-site.png") no-repeat 0 0 !important; }</style>';
}
add_action('login_head', 'LoginLogo');
add_filter('login_headerurl', create_function('', 'return get_home_url();'));
add_filter('login_headertitle', create_function('', 'return false;'));

// Register widgets
function besttravel_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'besttravel_widgets_init' ),
		'id'            => 'sidebar',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_widget('qTranslateXWidgetCustom');
}

class qTranslateXWidgetCustom extends WP_Widget {
	function __construct() {
		$widget_ops = array();
		parent::__construct('qtranslate', __('', 'qtranslate'), $widget_ops);
	}

	function widget($args, $instance) {
		extract($args);
		qtranxf_generateLanguageSelectCodeCustom($instance, $this->id);
	}
}

function qtranxf_generateLanguageSelectCodeCustom($args = array(), $id='') {
	global $q_config;

	echo '<div class="header-lang-dropdown dropdown">';
	echo '<a class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">';
	echo '' . $q_config['language_name'][$q_config['language']] . ' <span class="caret"></span>';
	echo '</a>';
	echo '<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">';
	foreach(qtranxf_getSortedLanguages() as $language) {
		echo '<li><a href="' . qtranxf_convertURL($url, $language, false, true) . '">' . $q_config['language_name'][$language] . '</a></li>';

	}
	echo '</ul>';
	echo '</div>'.PHP_EOL;
}
add_action('widgets_init', 'besttravel_widgets_init');

// Breadcrumbs 
function the_breadcrumb() {
    $text['home'] = get_the_title(244); // текст ссылки "Главная"
    $text['category'] = '%s'; // текст для страницы рубрики
    $text['search'] = 'Результаты поиска по запросу "%s"'; // текст для страницы с результатами поиска
    $text['tag'] = 'Записи с тегом "%s"'; // текст для страницы тега
    $text['author'] = 'Статьи автора %s'; // текст для страницы автора
    $text['404'] = 'Ошибка 404'; // текст для страницы 404
    $text['page'] = 'Страница %s'; // текст 'Страница N'
    $text['cpage'] = 'Страница комментариев %s'; // текст 'Страница комментариев N'

    if ( is_category() ) {
        $pageName = single_cat_title('', false);
    } elseif ( is_single() || is_page() ) {
        $pageName = get_the_title();
    } else {
        $pageName = get_the_title();
    }

    $wrap_before = '
    <div class="breadcrumb-block">
        <ul>'; // открывающий тег обертки
            $wrap_after = '
        </ul>
    </div>'; // закрывающий тег обертки
    $sep = ''; // разделитель между "крошками"
    $sep_before = ''; // тег перед разделителем
    $sep_after = ''; // тег после разделителя
    $show_home_link = 1; // 1 - показывать ссылку "Главная", 0 - не показывать
    $show_on_home = 0; // 1 - показывать "хлебные крошки" на главной странице, 0 - не показывать
    $show_current = 1; // 1 - показывать название текущей страницы, 0 - не показывать
    $before = '<li>'; // тег перед текущей "крошкой"
    $after = '</li>'; // тег после текущей "крошки"
    /* === КОНЕЦ ОПЦИЙ === */

    global $post;
    $home_link = home_url('/');
    $link_before = '';
    $link_after =  '';
    $link_attr = ' itemprop="url"';
    $link_in_before = '';
    $link_in_after = '';
    $link = $link_before . '<li><a class="after" href="%1$s"' . $link_attr . '>' . $link_in_before . '%2$s' . $link_in_after . '</a></li>' . $link_after;
    $frontpage_id = get_option('page_on_front');
    $parent_id = $post->post_parent;
    $sep = ' ' . $sep_before . $sep . $sep_after . ' ';

    if (is_home() || is_front_page()) {

        if ($show_on_home) echo $wrap_before . '<li><a class="after" itemprop="title" href="' . $home_link . '">' . $text['home'] . '</a></li>' . $wrap_after;

    } else {

        echo $wrap_before;
        if ($show_home_link) echo sprintf($link, $home_link, $text['home']);

        if ( is_category() ) {
            $cat = get_category(get_query_var('cat'), false);
            if ($cat->parent != 0) {
                $cats = get_category_parents($cat->parent, TRUE, $sep);
                $cats = preg_replace("#^(.+)$sep$#", "$1", $cats);
                $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<li><a$1' . $link_attr .' class="after">' . $link_in_before . '$2' . $link_in_after .'</a></li>' . $link_after, $cats);
                if ($show_home_link) echo $sep;
                echo $cats;
            }
            if ( get_query_var('paged') ) {
                $cat = $cat->cat_ID;
				echo $sep . $before . sprintf($text['category'], single_cat_title('', false)) . $after;
                //echo $sep . sprintf($link, get_category_link($cat), get_cat_name($cat)) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_current) echo $sep . $before . sprintf($text['category'], single_cat_title('', false)) . $after;
            }

        } elseif ( is_search() ) {
            if (have_posts()) {
                if ($show_home_link && $show_current) echo $sep;
                if ($show_current) echo $before . sprintf($text['search'], get_search_query()) . $after;
            } else {
                if ($show_home_link) echo $sep;
                echo $before . sprintf($text['search'], get_search_query()) . $after;
            }

        } elseif ( is_day() ) {
            if ($show_home_link) echo $sep;
            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $sep;
            echo sprintf($link, get_month_link(get_the_time('Y'), get_the_time('m')), get_the_time('F'));
            if ($show_current) echo $sep . $before . get_the_time('d') . $after;

        } elseif ( is_month() ) {
            if ($show_home_link) echo $sep;
            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y'));
            if ($show_current) echo $sep . $before . get_the_time('F') . $after;

        } elseif ( is_year() ) {
            if ($show_home_link && $show_current) echo $sep;
            if ($show_current) echo $before . get_the_time('Y') . $after;

        } elseif ( is_single() && !is_attachment() ) {
            if ($show_home_link) echo $sep;
            if ( get_post_type() != 'post' ) {
                $post_type = get_post_type_object(get_post_type());
                $slug = $post_type->rewrite;
                printf($link, $home_link . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
                if ($show_current) echo $sep . $before . get_the_title() . $after;
            } else {
                $cat = get_the_category(); $cat = $cat[0];
                $cats = get_category_parents($cat, TRUE, $sep);
                if (!$show_current || get_query_var('cpage')) $cats = preg_replace("#^(.+)$sep$#", "$1", $cats);
                $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<li><a$1' . $link_attr .' class="after">' . $link_in_before . '$2' . $link_in_after .'</a></li>' . $link_after, $cats);
                echo $cats;
                if ( get_query_var('cpage') ) {
                    echo $sep . sprintf($link, get_permalink(), get_the_title()) . $sep . $before . sprintf($text['cpage'], get_query_var('cpage')) . $after;
                } else {
                    if ($show_current) echo $before . get_the_title() . $after;
                }
            }

            // custom post type
        } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
            $post_type = get_post_type_object(get_post_type());

			if($post_type->name == 'usefull_post') {
				if(l()) {
					$post_type->label = 'שימושי למטיילים';
				} else {
					$post_type->label = 'Полезное для туристов';
				}
			}		
			
            if ( get_query_var('paged') ) {
                 echo $sep . $before . $post_type->label . $after;
				// echo $sep . sprintf($link, get_post_type_archive_link($post_type->name), $post_type->label) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_current) echo $sep . $before . $post_type->label . $after;
            }

        } elseif ( is_attachment() ) {
            if ($show_home_link) echo $sep;
            $parent = get_post($parent_id);
            $cat = get_the_category($parent->ID); $cat = $cat[0];
            if ($cat) {
                $cats = get_category_parents($cat, TRUE, $sep);
                $cats = preg_replace('#<li><a([^>]+)>([^<]+)<\/a>#', $link_before . '<li><a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a></li>' . $link_after, $cats);
                echo $cats;
            }
            printf($link, get_permalink($parent), $parent->post_title);
            if ($show_current) echo $sep . $before . get_the_title() . $after;

        } elseif ( is_page() && !$parent_id ) {
            if ($show_current) echo $sep . $before . get_the_title() . $after;

        } elseif ( is_page() && $parent_id ) {
            if ($show_home_link) echo $sep;
            if ($parent_id != $frontpage_id) {
                $breadcrumbs = array();
                while ($parent_id) {
                    $page = get_page($parent_id);
                    if ($parent_id != $frontpage_id) {
                        $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
                    }
                    $parent_id = $page->post_parent;
                }
                $breadcrumbs = array_reverse($breadcrumbs);
                for ($i = 0; $i < count($breadcrumbs); $i++) {
                    echo $breadcrumbs[$i];
                    if ($i != count($breadcrumbs)-1) echo $sep;
                }
            }
            if ($show_current) echo $sep . $before . get_the_title() . $after;

        } elseif ( is_tag() ) {
            if ( get_query_var('paged') ) {
                $tag_id = get_queried_object_id();
                $tag = get_tag($tag_id);
                echo $sep . sprintf($link, get_tag_link($tag_id), $tag->name) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_current) echo $sep . $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
            }

        } elseif ( is_author() ) {
            global $author;
            $author = get_userdata($author);
            if ( get_query_var('paged') ) {
                if ($show_home_link) echo $sep;
                echo sprintf($link, get_author_posts_url($author->ID), $author->display_name) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_home_link && $show_current) echo $sep;
                if ($show_current) echo $before . sprintf($text['author'], $author->display_name) . $after;
            }

        } elseif ( is_404() ) {
            if ($show_home_link && $show_current) echo $sep;
            if ($show_current) echo $before . $text['404'] . $after;

        } elseif ( has_post_format() && !is_singular() ) {
            if ($show_home_link) echo $sep;
            echo get_post_format_string( get_post_format() );
        }

        echo $wrap_after;

    }
}

// Pagination
function kama_pagenavi( $before = '', $after = '', $echo = true, $args = array(), $wp_query = null ) {
    if( ! $wp_query )
        global $wp_query;

    $default_args = array(
        'text_num_page'   => '', // Текст перед пагинацией. {current} - текущая; {last} - последняя (пр. 'Страница {current} из {last}' получим: "Страница 4 из 60" )
        'num_pages'       => 10, // сколько ссылок показывать
        'step_link'       => 10, // ссылки с шагом (значение - число, размер шага (пр. 1,2,3...10,20,30). Ставим 0, если такие ссылки не нужны.
        'dotright_text'   => '…', // промежуточный текст "до".
        'dotright_text2'  => '…', // промежуточный текст "после".
        'back_text'       => '<i class="fa fa-caret-left" aria-hidden="true"></i>', // текст "перейти на предыдущую страницу". Ставим 0, если эта ссылка не нужна.
        'next_text'       => '<i class="fa fa-caret-right" aria-hidden="true"></i>', // текст "перейти на следующую страницу". Ставим 0, если эта ссылка не нужна.
        'first_page_text' => '', // текст "к первой странице". Ставим 0, если вместо текста нужно показать номер страницы.
        'last_page_text'  => '', // текст "к последней странице". Ставим 0, если вместо текста нужно показать номер страницы.
    );

    $default_args = apply_filters('kama_pagenavi_args', $default_args );

    $args = array_merge( $default_args, $args );

    extract( $args );

    $posts_per_page = (int) $wp_query->query_vars['posts_per_page'];
    $paged          = (int) $wp_query->query_vars['paged'];
    $max_page       = $wp_query->max_num_pages;

    if( $max_page <= 1 )
        return false;

    if( empty( $paged ) || $paged == 0 )
        $paged = 1;

    $pages_to_show = intval( $num_pages );
    $pages_to_show_minus_1 = $pages_to_show-1;

    $half_page_start = floor( $pages_to_show_minus_1/2 ); //сколько ссылок до текущей страницы
    $half_page_end = ceil( $pages_to_show_minus_1/2 ); //сколько ссылок после текущей страницы

    $start_page = $paged - $half_page_start; //первая страница
    $end_page = $paged + $half_page_end; //последняя страница (условно)

    if( $start_page <= 0 )
        $start_page = 1;
    if( ($end_page - $start_page) != $pages_to_show_minus_1 )
        $end_page = $start_page + $pages_to_show_minus_1;
    if( $end_page > $max_page ) {
        $start_page = $max_page - $pages_to_show_minus_1;
        $end_page = (int) $max_page;
    }

    if( $start_page <= 0 )
        $start_page = 1;

    //выводим навигацию
    $out = '';

    $link_base = str_replace( 99999999, '___', get_pagenum_link( 99999999 ) );
    $first_url = get_pagenum_link( 1 );
    if( false === strpos( $first_url, '?') )
        $first_url = user_trailingslashit( $first_url );

    $out .= $before . " <div class=\"pagination-center\">
        <ul class=\"pagination\">\n";

    if( $text_num_page ){
        $text_num_page = preg_replace( '!{current}|{last}!', '%s', $text_num_page );
        $out.= sprintf( "<span class='pages'>$text_num_page</span> ", $paged, $max_page );
    }
    // назад
    if ( $back_text && $paged != 1 )
        $out .= '<li><a href="'. ( ($paged-1)==1 ? $first_url : str_replace( '___', ($paged-1), $link_base ) ) .'">'. $back_text .'</a></li>';
		else $out .= '<li class="disabled"><a>'. $back_text .'</a></li>';
    // в начало
    if ( $start_page >= 2 && $pages_to_show < $max_page ) {
        $out.= '<li><a href="'. $first_url .'">'. ( $first_page_text ? $first_page_text : 1 ) .'</a></li>';
        if( $dotright_text && $start_page != 2 ) $out .= '<span class="page-numbers dots">'. $dotright_text .'</span>';
    }
    // пагинация
    for( $i = $start_page; $i <= $end_page; $i++ ) {
        if( $i == $paged )
            $out .= '<li class="active"><a href="'. str_replace( '___', $i, $link_base ) .'">'. $i .'</a></li>';
        elseif( $i == 1 )
            $out .= '<li><a href="'. $first_url .'">1</a></li>';
        else
            $out .= '<li><a href="'. str_replace( '___', $i, $link_base ) .'">'. $i .'</a></li>';
    }

    $dd = 0;
    if ( $step_link && $end_page < $max_page ){
        for( $i = $end_page+1; $i<=$max_page; $i++ ) {
            if( $i % $step_link == 0 && $i !== $num_pages ) {
                if ( ++$dd == 1 )
                    $out.= '<span class="page-numbers dots">'. $dotright_text2 .'</span>';
					$out.= '<a href="'. str_replace( '___', $i, $link_base ) .'">'. $i .'</a> ';
            }
        }
    }
    // в конец
    if ( $end_page < $max_page ) {
        if( $dotright_text && $end_page != ($max_page-1) )
            $out.= '<span class="page-numbers dots">'. $dotright_text2 .'</span>';
            $out.= '<li><a href="'. str_replace( '___', $max_page, $link_base ) .'">'. ( $last_page_text ? $last_page_text : $max_page ) .'</a></li>';
    }
    // вперед
    if ( $next_text && $paged != $end_page )
        $out.= '<li><a href="'. str_replace( '___', ($paged+1), $link_base ) .'">'. $next_text .'</a></li>';
		else $out .= '<li class="disabled"><a>'. $next_text .'</a></li>';
	
	
    $out .= "</ul>
    </div>". $after ."\n";

    $out = apply_filters('kama_pagenavi', $out );

    if( $echo )
        return print $out;

    return $out;
}

/*// Category parent templates
function wp_parent_category_template() {
    if (!is_category()) return true; 

    $cat = get_category(get_query_var('cat'));
     while ($cat && !is_wp_error($cat)) {
        $template = TEMPLATEPATH . "/category-{$cat->slug}.php";
        if (file_exists($template)) {
            load_template($template);
            exit;
        }
        $cat = $cat->parent ? get_category($cat->parent) : false;
    }
}
add_action('template_redirect', 'wp_parent_category_template');*/

function decOfNum($n, $s1, $s2, $s3, $b = false){
	$m = $n % 10; $j = $n % 100;
	if($m == 0 || $m >= 5 || ($j >= 10 && $j <= 20)) return $n . ' ' . $s3;
	if($m >= 2 && $m <= 4) return  $n . ' ' . $s2;
	return $n . ' ' . $s1;
}

function l() {
	$language = qtrans_getLanguage();
	if($language == 'il') {
		$result = 1;
	} else {
		$result = 0;
	}
	return $result;
}

function l_rtl() {
	$language = qtrans_getLanguage();
	if($language == 'il') {
		$result = '-rtl';
	} else {
		$result = '';
	}
	return $result;
}

add_filter('body_class','custom_body_class');
function custom_body_class( $classes ) {

	if( l() )
		$classes[] = 'rtl';

	return $classes;
}

function my_pre_get_posts($query) {
	// do not modify queries in the admin
	if( is_admin() ) {		
		return $query;		
	}	
	
	if(isset($query->tax_query->queries[0]['taxonomy']) && $query->tax_query->queries[0]['taxonomy'] == 'usefull') {	
		$query->set('posts_per_page', 16); 	
		// $query->set('orderby', 'meta_value');	
		// $query->set('meta_key', 'start_date');	 
		// $query->set('order', 'DESC'); 
	}
	
	// return
	return $query;
}
add_action('pre_get_posts', 'my_pre_get_posts');