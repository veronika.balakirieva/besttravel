<?php get_header(); ?>

<div class="intro-top-block">
	<div class="container">
		<h1 class="title text-uppercase"><?php the_title(); ?></h1>
		<?php the_breadcrumb(); ?>
	</div>
</div>

<div class="content-wrap">
	<!-- section -->
	<div class="content-section blog-content-article">
		<div class="container">
			<h3 class="text-uppercase text-center"><?php echo the_field('title'); ?></h3>
			<h4 class="text-center"><?php echo the_field('subtitle'); ?></h4>
			<p class="text-center"><?php echo the_field('description'); ?></p>
			
			<div class="mobile-tab-col">
				<?php /*<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active">
						<button href="#Mtab1" class="btn btn-round" aria-controls="home" role="tab" data-toggle="tab">Medpay</button>
					</li>
					<li role="presentation">
						<button href="#Mtab2" class="btn btn-round" aria-controls="profile" role="tab" data-toggle="tab">Passportcard</button>
					</li>
				</ul>*/ ?>
				<div class="row vdivide pt15 tab-content">
					<?php 
						$wp_query = new WP_Query(array(
							'post_type' => array('innovatsii'),
							'orderby' => 'id',
							'order' => 'ASC',
							'posts_per_page' => 2
						));
					?>	
					<?php while($wp_query->have_posts()): $wp_query->the_post(); ?>
					<div role="tabpane<?php echo $wp_query->ID(); ?>" id="Mtab<?php echo $wp_query->ID(); ?>" class="tab-pane active col-sm-6">
						<div class="ins-item-col">
							<div class="text-center mb30">
								<?php echo get_the_post_thumbnail($wp_query->ID); ?>
							</div>
							<p><?php echo apply_filters('the_content', get_post_field('post_content',  $wp_query->ID)); ?></p>
							<div class="text-center">
								<a href="<?php echo get_field('link', $wp_query->ID); ?>"  target="_blank" class="btn btn-primary"><?php the_field('text_strahovanii_1', 'option'); ?></a>
							</div>
						</div>
					</div>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
	</div>

	<?php get_template_part('template-parts/insurance-companies'); ?>
</div>

<?php get_footer(); ?>	