<?php if(empty($_POST)) wp_redirect(esc_url(home_url('/')), 301); ?>
<?php get_header(); ?>
<!-- [* *** SUB PAGE INTRO START *** *] -->
<div class="intro-top-block">
	<div class="container">
		<h1 class="title text-uppercase"><?php the_title(); ?></h1>
		<?php the_breadcrumb(); ?>
	</div>
</div>
<!-- [* *** SUB PAGE INTRO AND *** *] -->
<?php
	$to_date = validate($_POST['to_date']);
	$from_date = validate($_POST['from_date']);
	$number_of_insured = validate($_POST['number_of_insured']);
	$birthday = (array)$_POST['birthday'];
	$question = validate($_POST['question']);
	$firstname = validate($_POST['firstname']);
	$phone = validate($_POST['phone']);
	$baggage = $_POST['baggage'] == 'on' ? 'Нет' : 'Да';

	if(!empty($firstname) && !empty($phone) && !empty($question)) {
		
		global $wpdb;
		$html = '';
		
		foreach($birthday as $item) {
			$html .= $item . '<br>';
		}
		
		$sql = "INSERT INTO " . $wpdb->prefix . "request SET 
			from_date = '" . $from_date . "', 
			to_date = '" . $to_date . "',
			number_of_insured = '" . $number_of_insured . "',
			firstname = '" . $firstname . "',
			phone = '" . $phone . "',
			baggage = '" . $baggage . "', 
			birthday = '" . $html . "', 
			date_created = NOW()";
		$wpdb->query($sql);
		
		// Send email
		$to = '';
		$subject = 'Besttravel - Заявка на страхование';
		$body = 'Дата выезда: ' . $to_date . '<br />';
		$body.= 'Дата приезда: ' . $from_date . '<br />';
		$body.= 'Количество застрахованных: ' . $number_of_insured . '<br />';
		$body.= 'Дата рождения: ' . $html . '';
		$body.= 'Багаж: ' . $baggage . '<br />';
		$body.= 'Имя: ' . $firstname . '<br />';
		$body.= 'Телефон: ' . $phone . '<br />';
		$headers = array(
			'Content-Type: text/html; charset=UTF-8',
			'From: Besttravel <info@besttravel.co.il>'
		);
		wp_mail( $to, $subject, $body, $headers );
		
		//
		get_template_part('template-parts/insurance-success');	
		
		unset($_POST);
	} else {		
		
?>

<!-- [* *** CONTENT START *** *] -->
<div class="content-wrap">
	<!-- section -->
	<div class="content-section">
		<div class="container">
			<div class="content-section-title">
				<h3 class="text-uppercase"><?php the_field('text_index_1', 'option'); ?> <?php the_field('text_index_2', 'option'); ?></h3>
				<div class="sub-title-small">
					<p><?php the_field('text_calculate_2', 'option'); ?>: <span class="color-blue"><?php echo $to_date; ?></span>, <?php the_field('text_calculate_1', 'option'); ?>: <span class="color-blue"><?php echo $from_date; ?></span>,<br>
					<?php the_field('text_calculate_3', 'option'); ?>: <span class="color-blue"><?php echo getDiffDay($to_date, $from_date); ?></span>, <?php the_field('text_calculate_4', 'option'); ?>: <span class="color-blue"><?php echo $number_of_insured; ?></span><br>
					<a href="<?php echo esc_url(home_url('/')); ?>"><?php the_field('text_calculate_12', 'option'); ?></a></p>
				</div>
			</div>
			<?php 
				$wp_query = new WP_Query(array(
					'post_type' => array('insurance_companies'),
					'orderby' => 'id',
					'order' => 'ASC',
					'posts_per_page' => 5
				));
			?>				
			<div class="mt30 visible-lg">
				<div class="table-responsive">
					<table class="table table-hover table-bordered table-bordered-dotted">
						<thead>
							<tr class="tr">
								<th width="600px"><?php the_field('text_calculate_15', 'option'); ?></th>
								<?php if($wp_query->have_posts()): while($wp_query->have_posts()): $wp_query->the_post(); ?>
								<td>
									<a href="<?php echo get_field('link_on_site', $wp_query->ID); ?>" target="_blank">
										<?php echo get_the_post_thumbnail($wp_query->ID, 'besttravel-price-compare', array('class' => 'company-img')); ?>
									</a>
								</td>
								<?php endwhile; endif; ?>  
							</tr>
						</thead>
						<tbody>
							<tr class="tr">
								<th><?php the_field('text_calculate_16', 'option'); ?></th>
								<?php if($wp_query->have_posts()): while($wp_query->have_posts()): $wp_query->the_post(); ?>
								<td><span class="price">
								<?php 
									$data = array(
										'id' => get_the_ID(),
										'birthday' => $birthday,
										'to_date' => $to_date,
										'from_date' => $from_date,
										'baggage' => $baggage
									);								
									echo calculatorPrice($data); 
								?>
								</span></td>
								<?php endwhile; endif; ?>  
							</tr>
							<tr class="tr">
								<th>
									<?php the_field('text_calculate_17', 'option'); ?>
								</th>
								<?php if($wp_query->have_posts()): while($wp_query->have_posts()): $wp_query->the_post(); ?>
								<td>
									<a href="<?php echo get_field('link_on_site', $wp_query->ID); ?>" target="_blank" class="btn btn-primary"><?php the_field('text_calculate_19', 'option'); ?></a>
								</td>
								<?php endwhile; endif; ?> 
							</tr>
						</tbody>
					</table>
				</div>
				<div class="table-responsive p-clear" style="margin-top:-16px;">
					<?php echo apply_filters('the_content', get_post_field('post_content',  260));?>
				</div>
				
				<h5 class="text-center text-uppercase color-blue"><?php the_field('text_calculate_14', 'option'); ?></h5>
				<div class="table-responsive p-clear">
					<?php echo apply_filters('the_content', get_post_field('post_content',  262));?>
				</div>
				<div class="table-responsive" style="margin-top:-16px">
					<table class="table table-condensed table-hover table-bordered table-bordered-dotted">		
						<tbody>
							<tr>
								<th><?php the_field('text_calculate_13', 'option'); ?></th>
								<?php if($wp_query->have_posts()): while($wp_query->have_posts()): $wp_query->the_post(); ?>
								<td>
									<a data-toggle="tooltip" data-placement="auto" data-html="true" title="<?php echo get_field('benefits', $wp_query->ID); ?>">
										<img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/link-like.png" alt="">
									</a>
								</td>
								<?php endwhile; endif; ?> 
							</tr>
							<tr>
								<th><?php the_field('text_calculate_18', 'option'); ?></th>
								<?php if($wp_query->have_posts()): while($wp_query->have_posts()): $wp_query->the_post(); ?>
								<td>
									<?php $pdf_file = get_field('pdf', $wp_query->ID); ?>
									<a href="<?php echo $pdf_file['url']; ?>" target="_blank">
										<img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/link-pdf.png" alt="">
									</a>
								</td>
								<?php endwhile; endif; ?> 
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="mt30 hidden-lg">
				<div class="row">
					<?php if($wp_query->have_posts()): while($wp_query->have_posts()): $wp_query->the_post(); ?>
					<div class="col-sm-15">
						<div class="item-company-block">
							<div class="descr">
								<?php echo get_the_post_thumbnail($wp_query->ID, 'besttravel-price-compare', array('class' => 'company-img')); ?>
							</div>
							<div class="price">								
								<?php 
									$data = array(
										'id' => get_the_ID(),
										'birthday' => $birthday,
										'to_date' => $to_date,
										'from_date' => $from_date,
										'baggage' => $baggage
									);								
									echo calculatorPrice($data); 
								?>
							</div>
							<a href="<?php echo get_field('link_on_site', $wp_query->ID); ?>" target="_blank" class="btn btn-primary"><?php the_field('text_calculate_19', 'option'); ?></a>
						</div>
					</div>
					<?php endwhile; endif; ?> 
				</div>
			</div>
		</div>
	</div>

	<?php get_template_part('template-parts/insurance-companies'); ?>
</div>

<?php		
	}

	function getDiffDay($to, $from) {
		$from = strtotime($from . ' 23:59:59');
		$to = strtotime($to . ' 00:00:00');
		$timeDiff = (int)$from - (int)$to;
		$diffDays = round($timeDiff / 86400);
		
		return $diffDays;
	}
	
	function calculatorPrice($data) {
		// $all = get_fields();
		
		foreach($data['birthday'] as $item) {
			$age = getFullYears($item);

			if($data['id'] == 237) {
				if($age >= 0 && $age <= 17) {
					$summ = get_field('company_237_0_17');
				} else if($age >= 18 && $age <= 60) {
					$summ = get_field('company_237_18_60');
				} else if($age >= 61 && $age <= 65) {
					$summ = get_field('company_237_61_65');
				} else if($age >= 66 && $age <= 74) {
					$summ = get_field('company_237_66_74');
				} else if($age >= 75 && $age <= 85) {
					$summ = get_field('company_237_75_85');
				} else if($age >= 86 && $age <= 120) {
					$summ = get_field('company_237_86_120');
				}
				if($data['baggage'] == 'Нет') { 
					$baggage = get_field('company_237_baggage') * getDiffDay($data['to_date'], $data['from_date']);
				} else {
					$baggage = 0;
				}
			} else if($data['id'] == 241) {
				if($age >= 0 && $age <= 17) {
					$summ = get_field('company_241_0_17');
				} else if($age >= 18 && $age <= 60) {
					$summ = get_field('company_241_18_60');
				} else if($age >= 61 && $age <= 65) {
					$summ = get_field('company_241_61_65');
				} else if($age >= 66 && $age <= 74) {
					$summ = get_field('company_241_66_70');
				} else if($age >= 75 && $age <= 85) {
					$summ = get_field('company_241_71_75');
				} else if($age >= 76 && $age <= 79) {
					$summ = get_field('company_241_76_79');
				} else if($age >= 80 && $age <= 84) {
					$summ = get_field('company_241_80_84');
				}
				if($data['baggage'] == 'Нет') { 
					$baggage = get_field('company_241_baggage') * getDiffDay($data['to_date'], $data['from_date']);
				} else {
					$baggage = 0;
				}									
			} else if($data['id'] == 250) {
				if($age >= 0 && $age <= 17) {
					$summ = get_field('company_250_0_17');
				} else if($age >= 18 && $age <= 60) {
					$summ = get_field('company_250_18_60');
				} else if($age >= 61 && $age <= 65) {
					$summ = get_field('company_250_61_65');
				} else if($age >= 66 && $age <= 70) {
					$summ = get_field('company_250_66_70');
				} else if($age >= 71 && $age <= 75) {
					$summ = get_field('company_250_71_75');
				} else if($age >= 76 && $age <= 85) {
					$summ = get_field('company_250_76_85');
				}
				if($data['baggage'] == 'Нет') { 
					$baggage = get_field('company_250_baggage') * getDiffDay($data['to_date'], $data['from_date']);
				} else {
					$baggage = 0;
				}									
			} else if($data['id'] == 251) {
				if($age >= 0 && $age <= 16) {
					$summ = get_field('company_251_0_16');
				} else if($age >= 17 && $age <= 60) {
					$summ = get_field('company_251_17_60');
				} else if($age >= 61 && $age <= 75) {
					$summ = get_field('company_251_61_75');
				} else if($age >= 76 && $age <= 80) {
					$summ = get_field('company_251_76_80');
				} else if($age >= 81 && $age <= 85) {
					$summ = get_field('company_251_81_85');
				}

				if($data['baggage'] == 'Нет') { 			
					$baggage_total += $summ * getDiffDay($data['to_date'], $data['from_date']);
					$baggage = $baggage_total * get_field('company_251_baggage') / 100;
				} else {
					$baggage = 0;
				} 
			} else if($data['id'] == 252) {
				if($age >= 0 && $age <= 17) {
					$summ = get_field('company_252_0_17');
				} else if($age >= 18 && $age <= 40) {
					$summ = get_field('company_252_18_40');
				} else if($age >= 41 && $age <= 50) {
					$summ = get_field('company_252_41_50');
				} else if($age >= 51 && $age <= 60) {
					$summ = get_field('company_252_51_60');
				} else if($age >= 61 && $age <= 70) {
					$summ = get_field('company_252_61_70');
				} else if($age >= 71 && $age <= 75) {
					$summ = get_field('company_252_71_75');
				} else if($age >= 76 && $age <= 80) {
					$summ = get_field('company_252_76_80');
				} else if($age >= 81 && $age <= 85) {
					$summ = get_field('company_252_81_85');
				} else if($age >= 86 && $age <= 90) {
					$summ = get_field('company_252_86_90');
				}				
				if($data['baggage'] == 'Нет') { 
					$baggage = get_field('company_252_baggage') * getDiffDay($data['to_date'], $data['from_date']);
				} else {
					$baggage = 0;
				}	
			}			

			$summ_day = $summ * getDiffDay($data['to_date'], $data['from_date']);
			$summ_total = $summ_day - $baggage;
			$total += $summ_total;			
		}
		
		if($total == '0.00')
			return '-';
		else
			return '$' . number_format($total, 2);
	}
	
	function getFullYears($birthdayDate) {
		$datetime = new DateTime($birthdayDate);
		$interval = $datetime->diff(new DateTime(date("Y-m-d")));
		return $interval->format("%Y");
	}	
	
	function validate($str) {
		$str = stripslashes($str);
		$str = htmlspecialchars($str);
		
		return $str;
	}
?>

<?php get_footer(); ?>	