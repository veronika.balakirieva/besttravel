<?php get_header(); ?>

<!-- [* *** SUB PAGE INTRO START *** *] -->
<div class="intro-top-block">
	<div class="container">
		<h1 class="title text-uppercase"><?php single_cat_title(); ?></h1>
		<?php the_breadcrumb(); ?>
	</div>
</div>
<!-- [* *** SUB PAGE INTRO AND *** *] -->

<!-- [* *** CONTENT START *** *] -->
<div class="content-wrap">
	<!-- section -->
	<div class="content-section">
		<div class="container">
		<?php if(!empty($wp_query)): ?>
			<?php $active_terms = get_queried_object(); ?>
				<div class="sub-page-link">
					<ul>
						<li<?php if($active_terms->term_id == 31) { ?> class="active"<?php } ?>><a href="<?php echo esc_url(home_url('/articles/')); ?>" class="btn btn-round"><?php the_field('text_all', 'option'); ?></a></li>
						<?php	  
							$categories = get_categories(array(
								'type' => 'post',
								'orderby' => 'id',
								'order' => 'ASC',
								'taxonomy' => 'category',
								'parent' => 31
							)); 
							foreach($categories as $category):
						?>					
							<li<?php if($category->term_id == $active_terms->term_id) { ?> class="active"<?php } ?>><a href="<?php echo get_category_link($category->term_id); ?>" class="btn btn-round"><?php echo $category->name; ?></a></li>
						<?php endforeach; ?>
					</ul>
				</div>
		
				<?php if(have_posts()): while(have_posts()): the_post(); ?>
				<div class="blog_post_preview">
					<div class="row">
						<div class="col-sm-5"> 
							<div class="blog_post_media">
								<?php the_post_thumbnail('besttravel-articles-category'); ?>
							</div>
						</div>
						<div class="col-sm-7">
							<div class="blog_post_cont">
								<h5 class="blogpost_title">
									<a href="<?php the_permalink();?>"><?php the_title(); ?></a>
								</h5>
								<div class="listing_meta">
									<?php $cat = get_the_category(); ?>
									<span><?php the_field('text_articles_1', 'option'); ?>: <a href="<?php echo esc_url(get_category_link($cat[0]->term_id)); ?>"><?php echo $cat[0]->cat_name; ?></a></span>, <span><?php the_field('text_articles_2', 'option'); ?>: <i><?php echo get_the_date('d.m.Y');?></i></span>
								</div>
								<p><?php echo wp_trim_words(get_the_excerpt(), 50, ' ...');?></p>
								<a href="<?php the_permalink();?>" class="btn btn-round"><?php the_field('text_articles_3', 'option'); ?></a>
							</div>
						</div>
					</div>
				</div>  
				<?php endwhile; endif; ?>		
			
				<?php 
					// Pagination
					kama_pagenavi(); 
				?>
			<?php else: ?>
				<div class="text-center"><?php the_field('text_not_found', 'option'); ?></div>			
			<?php endif; wp_reset_postdata(); ?>	
		</div>
	</div>

	<?php get_template_part('template-parts/insurance-companies'); ?>
</div>
<!-- [* *** CONTENT AND *** *] -->

<?php get_footer(); ?>	