<?php get_header(); ?>
<?
	$queried_object = get_queried_object(); 
	$taxonomy = $queried_object->taxonomy;
	$term_id = $queried_object->term_id;  
?>

<!-- [* *** SUB PAGE INTRO START *** *] -->
<div class="intro-top-block">
	<div class="container">
		<h1 class="title text-uppercase"><?php single_cat_title(); ?></h1>
		<?php the_breadcrumb(); ?>
	</div>
</div>
<!-- [* *** SUB PAGE INTRO AND *** *] -->

<!-- [* *** CONTENT START *** *] -->
<div class="content-wrap">
	<!-- section -->
	<div class="content-section">
		<div class="container">
			<div class="content-section-title">
				<h3 class="text-uppercase"><?php the_field('title', $queried_object); ?></h3>
				<div class="sub-title-small">
					<p><?php the_field('description', $queried_object); ?></p>
				</div>
			</div>

			<div class="mt10">
				<div class="row">
				<?php if(!empty($wp_query)): ?>		
					<?php $i = 0; if(have_posts()): while(have_posts()): the_post(); $i++; ?>		
					<div class="col-sm-6 col-md-3">
						<div class="item-listtop-block">
							<div class="img">
								<?php the_post_thumbnail('besttravel-useful-for-tourists'); ?>
							</div>
							<div class="descr">
								<h5 class="title">
									<a href="<?php the_field('link_on_site'); ?>" target="_blank"><?php the_title(); ?></a>
								</h5>								
								<p><?php echo wp_trim_words(apply_filters('the_content', get_post_field('post_content',  get_the_ID())), 15, ' ...'); ?></p>
								<a href="<?php the_field('link_on_site'); ?>" target="_blank" class=" btn btn-round"><?php the_field('text_look_site', 'option'); ?></a>
							</div>
						</div>
					</div>
					<?php if($i % 2 == 0 && $i % 4 != 0) { ?><div class="clearfix visible-sm"></div><?php } ?>
					<?php if($i % 4 == 0) { ?><div class="clearfix"></div><?php } ?>
					<?php endwhile; endif; ?>
					<div class="clearfix"></div>
					<?php 
						// Pagination
						kama_pagenavi(); 
					?>
				<?php else: ?>
					<div class="text-center"><?php the_field('text_not_found', 'option'); ?></div>			
				<?php endif; wp_reset_postdata(); ?>
				</div>
			</div>
		</div>
	</div>

	<?php get_template_part('template-parts/insurance-companies'); ?>
</div>
<!-- [* *** CONTENT AND *** *] -->

<?php get_footer(); ?>	