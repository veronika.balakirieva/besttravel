<?php get_header(); ?>

<!-- [* *** SUB PAGE INTRO START *** *] -->
<div class="intro-top-block">
	<div class="container">
		<h1 class="title text-uppercase">
			<?php $category = get_the_category(); ?>
			<?php echo $category[0]->cat_name; ?>
		</h1>
		<?php the_breadcrumb(); ?>
	</div>
</div>
<!-- [* *** SUB PAGE INTRO AND *** *] -->

<!-- [* *** CONTENT START *** *] -->
<div class="content-wrap">
	<!-- section -->
	<?php while(have_posts()): the_post(); ?>
	<div class="content-section blog-content-article">
		<div class="container">
			<div class="sub-page-link">
				<ul>
					<li><a href="<?php echo esc_url(home_url('/articles/')); ?>" class="btn btn-round"><?php the_field('text_all', 'option'); ?></a></li>
					<?php	  
						$categories = get_categories(array(
							'type' => 'post',
							'orderby' => 'id',
							'order' => 'ASC',
							'taxonomy' => 'category',
							'parent' => 31
						)); 
						foreach($categories as $category):
					?>					
						<li><a href="<?php echo get_category_link($category->term_id); ?>" class="btn btn-round"><?php echo $category->name; ?></a></li>
					<?php endforeach; ?>
				</ul>
			</div>

			<div class="content-section-title">
				<h3 class="text-uppercase"><?php the_title(); ?></h3>
				<div class="sub-title-small">
					<?php $cat = get_the_category(); ?>
					<p><?php the_field('text_articles_1', 'option'); ?>: <a href="<?php echo esc_url(get_category_link($cat[0]->term_id)); ?>"><?php echo $cat[0]->cat_name; ?></a>, <?php the_field('text_articles_2', 'option'); ?>: <span class="color-blue"><?php echo get_the_date('d.m.Y');?></span>,</p>
				</div>
			</div>			
			
			<?php the_content();?>			
		</div>
	</div>
	<?php endwhile;?>
	
	<?
		$category_ids = array();
		foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
		$args=array(
			'category__in' => $category_ids,
			'post__not_in' => array($post->ID),
			'showposts' => 2,
			'orderby' => 'date',
			'order' => 'DESC',
			'caller_get_posts' => 1
		);
		$other_articles_query = new WP_Query($args);
		if(isset($other_articles_query)) {	
	?>	
	<div class="content-section section-blog-category">
		<div class="container">
			<div class="content-section-title">
				<h3 class="text-uppercase"><?php the_field('text_articles_4', 'option'); ?></h3>
			</div>
	<?php
	if($other_articles_query->have_posts()):
		while($other_articles_query->have_posts()): $other_articles_query->the_post();
	?>
		<div class="blog_post_preview">
			<div class="row">
				<div class="col-sm-5"> 
					<div class="blog_post_media">
						<?php the_post_thumbnail('besttravel-articles-category'); ?>
					</div>
				</div>
				<div class="col-sm-7">
					<div class="blog_post_cont">
						<h5 class="blogpost_title">
							<a href="<?php the_permalink();?>"><?php the_title(); ?></a>
						</h5>
						<div class="listing_meta">
							<?php $cat = get_the_category(); ?>
							<span><?php the_field('text_articles_1', 'option'); ?>: <a href="<?php echo esc_url(get_category_link($cat[0]->term_id)); ?>"><?php echo $cat[0]->cat_name; ?></a></span>, <span><?php the_field('text_articles_2', 'option'); ?>: <i><?php echo get_the_date('d.m.Y');?></i></span>
						</div>
						<p><?php echo wp_trim_words(get_the_excerpt(), 50, ' ...'); ?></p>
						<a href="<?php the_permalink();?>" class="btn btn-round"><?php the_field('text_articles_3', 'option'); ?></a>
					</div>
				</div>
			</div>
		</div>  
	<?php	
		endwhile;
		wp_reset_query();
	endif;
	?>
		</div>
	</div>
	<?php } ?>				

	<div class="content-section">
		<div class="container">
			<div class="content-section-title">
				<h3 class="text-uppercase"><?php the_field('text_articles_5', 'option'); ?></h3>
			</div>
			<?php get_template_part('template-parts/articles-categories'); ?>
		</div>
	</div>
</div>
<!-- [* *** CONTENT AND *** *] -->
	
<?php get_footer(); ?>	