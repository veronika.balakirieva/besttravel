<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.png" rel="shortcut icon" type="image/x-icon">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!-- [* *** INTRO START *** *] -->
<?php if(is_front_page()) { ?><div class="intro-full-block"><?php } ?>
<!-- [* *** HEADER START *** *] -->
<header class="header-page navbar-default">
	<div class="container">
		<div class="row">
			<div class="col-xs-6 col-md-2">
				<!-- [* logo *] -->
				<a href="<?php echo esc_url(home_url('/')); ?>" class="header-logo">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-site.png" alt="<?php bloginfo('name'); ?>">
				</a>
			</div>
			<div class="col-md-7 hidden-xs hidden-sm">
				<!-- [* menu *] -->	
				<?php
					wp_nav_menu(array(
						'theme_location'  => 'menu-header',
						'menu'            => '', 
						'container'       => 'div', 
						'container_class' => 'header-menu', 
						'container_id'    => '',
						'menu_class'      => 'menu', 
						'menu_id'         => '',
						'echo'            => true,
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '<ul>%3$s</ul>',
						'depth'           => 0,
						'walker'          => '',
					));
				?>
			</div>
			<div class="col-xs-4 col-md-1 text-right">
				<a href="#" class="bvi-panel-open">
					<i class="fa fa-wheelchair" aria-hidden="true"></i>
				</a>				
				<!-- [* lang *] -->
				<?php the_widget('qTranslateXWidgetCustom');?>
			</div>
			<div class="col-md-2 hidden-xs hidden-sm text-right">
				<!-- [* phone block *] -->
				<div class="header_phone-block"><?php the_field('phone', 11); ?></div>
			</div>
			<div class="col-xs-2 visible-xs-block visible-sm-block">                    
			   <button type="button" class="navbar-toggle offcanvas-toggle offcanvas-toggle-close" data-toggle="offcanvas" data-target="#js-bootstrap-offcanvas">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
		</div>
	</div>

	<div class="navbar-offcanvas navbar-offcanvas-touch" id="js-bootstrap-offcanvas">
		<h4>Menu</h4>
		<?php
			wp_nav_menu(array(
				'theme_location'  => 'menu-header',
				'menu'            => '', 
				'container'       => 'div', 
				'container_class' => 'canvas-menu', 
				'container_id'    => '',
				'menu_class'      => 'menu', 
				'menu_id'         => '',
				'echo'            => true,
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul>%3$s</ul>',
				'depth'           => 0,
				'walker'          => '',
			));
		?>			
		<h4 style="margin:0;">Phone</h4>
		<div class="header_phone-block"><?php the_field('phone', 11); ?></div>
	</div>
</header>
<!-- [* *** HEADER AND *** *] -->