<?php get_header(); ?>

<!-- [* *** SUB PAGE INTRO START *** *] -->
<div class="intro-top-block">
	<div class="container">
		<h1 class="title text-uppercase"><?php single_cat_title(); ?></h1>
		<?php the_breadcrumb(); ?>
	</div>
</div>
<!-- [* *** SUB PAGE INTRO AND *** *] -->

<!-- [* *** CONTENT START *** *] -->
<div class="content-wrap">
	<!-- section -->
	<div class="content-section">
		<div class="container">
		<?php if(!empty($wp_query)): ?>
			<div class="sub-page-link">
				<ul>
					<li><a href="<?php echo esc_url(home_url('/faq/'));?>" class="btn btn-round"><?php the_field('text_all', 'option'); ?></a></li>
					<?php					
						$active_terms = get_queried_object();
						$categories = get_categories(array(
							'type' => 'post',
							'child_of' => 0,
							'orderby' => 'id',
							'order' => 'ASC',
							'taxonomy' => 'faq'
						)); 
						foreach($categories as $category):
					?>					
						<li<?php if($category->term_id == $active_terms->term_id) { ?> class="active"<?php } ?>><a href="<?php echo get_category_link($category->term_id); ?>" class="btn btn-round"><?php echo $category->name; ?></a></li>
					<?php endforeach; ?>
				</ul>
			</div>

			<!-- faq -->
			<div class="mt_20">
				<div class="panel-group accordion-wrap mb50" id="accordion" role="tablist" aria-multiselectable="true">
				<?php if(have_posts()): while(have_posts()): the_post(); ?>					
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="heading<?php echo $post->ID; ?>">
							<div class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $post->ID; ?>" aria-expanded="true" aria-controls="collapse<?php echo $post->ID; ?>">
									<?php echo get_field('question', $post->ID); ?>
								</a>
							</div>
						</div>
						<div id="collapse<?php echo $post->ID; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $post->ID; ?>">
							<div class="panel-body">
								<?php echo get_field('answer', $post->ID); ?>
							</div>
						</div>
					</div>
				<?php endwhile; endif; ?>	
				</div>
			</div>

			<?php 
				// Pagination
				kama_pagenavi(); 
			?>
		<?php else: ?>
			<div class="text-center"><?php the_field('text_not_found', 'option'); ?></div>			
		<?php endif; wp_reset_postdata(); ?>
		</div>
	</div>

	<?php get_template_part('template-parts/insurance-companies'); ?>
</div>
<!-- [* *** CONTENT AND *** *] -->
	
<?php get_footer(); ?>	