<?php get_header();  ?>

<!-- [* *** FORM INTRO START *** *] -->
<div class="intro-wrap-center">
	<?php get_template_part('template-parts/insurance-form'); ?>
</div>
<!-- [* *** FORM INTRO AND *** *] -->
</div>
<!-- [* *** INTRO AND *** *] -->

<!-- [* *** CONTENT START *** *] -->
<div class="content-wrap">
	<?php get_template_part('template-parts/usefull-categories'); ?>

	<!-- section -->
	<div class="content-section">
		<div class="container">
			<div class="content-section-title">
				<h3 class="text-uppercase"><?php echo get_the_title(218); ?></h3>
				<h4 class="sub-title"><?php echo get_field('title_sub', 218); ?></h4>
				<?php echo apply_filters('the_content', get_post_field('post_content',  218));?>
			</div>
		</div>
	</div>

	<!-- section -->
	<div class="content-section">
		<div class="container">
			<div class="content-section-title">
				<h3 class="text-uppercase"><?php the_field('text_index_6', 'option'); ?></h3>
			</div>
			<?php get_template_part('template-parts/articles-categories'); ?>
		</div>
	</div>

	<?php get_template_part('template-parts/faq-categories'); ?>

	<div class="content-section feedback-section margin-remove">
		<div class="container">
			<div class="content-section-title">
				<h3 class="text-uppercase"><?php the_field('text_index_9', 'option'); ?></h3>
				<h4 class="sub-title"><?php the_field('text_index_10', 'option'); ?></h4>
			</div>
			<div class="feedback-form">
				<?php if(l()) { ?>
					<?php echo do_shortcode('[contact-form-7 id="190" title="Форма на главной Остались вопросы? EL"]');?>		
				<?php } else { ?>
					<?php echo do_shortcode('[contact-form-7 id="189" title="Форма на главной Остались вопросы? RU"]');?>	
				<?php } ?>			
			</div>
		</div>
	</div>	
</div>
<!-- [* *** CONTENT AND *** *] -->

<?php get_footer(); ?>