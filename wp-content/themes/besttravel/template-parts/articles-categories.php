<div class="row">
	<?php	  
		$categories = get_categories(array(
			'type' => 'post',
			'orderby' => 'id',
			'order' => 'ASC',
			'taxonomy' => 'category',
			'parent' => 31,
			'number' => 4
		)); 
		foreach($categories as $category):
	?>
	<div class="col-sm-3 col-md-3">
		<a href="<?php echo get_category_link($category->term_id); ?>" class="service-item-col type1">			
			<div class="img-block" style="background: url('<?php echo wp_get_attachment_image_url(get_field('image_block', $category), 'besttravel-articles-category-block'); ?>');"></div>
			<div class="descr">
				<div class="title-col"> 
					<h5><?php echo $category->name; ?></h5>
				</div>
				<p><?php the_field('short_description_block', $category); ?></p>
			</div>
		</a>
	</div>
	<?php endforeach; ?>
</div>			