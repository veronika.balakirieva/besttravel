<?php
	$categories = get_categories(array(
		'type' => 'post',
		'orderby' => 'id',
		'order' => 'ASC',
		'taxonomy' => 'faq'
	)); 
?>
<?php if(!empty($categories)) { ?>
<div class="content-section">
	<div class="container">
		<div class="content-section-title">
			<h3 class="text-uppercase"><?php the_field('text_index_7', 'option'); ?></h3>
		</div>
		<div class="row">
			<?php	  
				foreach($categories as $category):
			?>
			<div class="col-sm-4 col-md-4">
				<div class="question-item-col">
					<div class="title-col">					
						<?php $image = get_field('image_block', $category); ?>
						<img class="icon" src="<?php echo $image['url']; ?>" alt="<?php echo $category->name; ?>">
						<h5><?php echo $category->name; ?></h5>
					</div>
					<div class="descr">
						<?php the_field('short_description_block', $category); ?>
					</div>
					<a href="<?php echo get_category_link($category->term_id); ?>" class="btn btn-round"><?php the_field('text_index_8', 'option'); ?></a>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
<?php } ?>