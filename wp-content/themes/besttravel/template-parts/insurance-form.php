<div class="container">
	<div class="big-intro-title text-center">
		<h1><?php the_field('text_index_1', 'option'); ?></h1>
		<h4><?php the_field('text_index_2', 'option'); ?></h4>
	</div>
	<div class="block-form-comparison">
		<form action="<?php the_permalink(14); ?>" method="post">
			<div class="step-item step-1">
				<div class="row row-form">
					<div class="col-sm-6">
						<div class="form-group calendar-group">
							<label for="to-date"><?php the_field('text_calculate_2', 'option'); ?>:</label>
							<input type="text" id="to-date" name="to_date" class="form-control" readonly>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group calendar-group">
							<label for="from-date"><?php the_field('text_calculate_1', 'option'); ?>:</label>
							<input type="text" id="from-date" name="from_date" class="form-control" disabled readonly>
						</div>
					</div>
				</div>
				<div class="form-content">
					<p><?php the_field('text_calculate_3', 'option'); ?>:  <span class="diff-day">0</span></p>
					<div class="row row-wrap">
						<div class="col-sm-6">
							<div class="row form-group clearfix">
								<label class="col-sm-8 col-md-8"><?php the_field('text_calculate_4', 'option'); ?>:</label>
								<div class="col-sm-4 col-md-4">
									<input name="number_of_insured" class="val_insurant" type="text" value="1" readonly>
								</div>
							</div>
						</div>
						<div class="col-sm-6 hasBirthday">
							<div class="form-group calendar-group">
								<label for="birthday"><?php the_field('text_calculate_5', 'option'); ?>:</label>
								<input id="birthday" type="text" name="birthday[]" class="form-control" readonly>
							</div>
						</div>
					</div>
					<div class="text-right">
						<a class="bootstrap-touchspin-reset"><?php the_field('text_calculate_6', 'option'); ?></a>
					</div>
					<div class="hasErrorDisplayNone"><?php the_field('text_error', 'option'); ?></div>
				</div>
				<div class="form-footer">
					<div class="checkbox-wrap">
						<input name="baggage" id="checkbox1" class="styled" type="checkbox">
						<label for="checkbox1">
							<?php the_field('text_calculate_7', 'option'); ?>
						</label>
					</div>
				</div>
				<div class="text-center">
					<a class="btn btn-primary step-1-next"><?php the_field('text_next', 'option'); ?></a>
				</div>				
			</div>
			<div class="step-item step-2">
				<div class="form-content text-center">
					<h5 class="text-uppercase color-bluelight"><?php the_field('text_calculate_8', 'option'); ?></h5>
					<p><?php the_field('text_calculate_9', 'option'); ?>:</p>
					<div class="mb20">
						<div class="radio-wrap radio-inline">
							<input type="radio" id="inlineRadio_yes" value="1" name="question">
							<label for="inlineRadio_yes"> <?php the_field('text_yes', 'option'); ?> </label>
						</div>
						<div class="radio-wrap radio-inline">
							<input type="radio" id="inlineRadio_no" value="0" checked name="question">
							<label for="inlineRadio_no"> <?php the_field('text_no', 'option'); ?> </label>
						</div>
					</div>
					<div class="row step-2-form">
						<div class="col-sm-5">
							<div class="form-group has-feedback has-mandatory">
								<input name="firstname" type="text" class="form-control" placeholder="<?php the_field('text_form_name', 'option'); ?>">
								<span class="form-control-feedback" aria-hidden="true">*</span>
							</div>
						</div>
						<div class="col-sm-5">
							<div class="form-group margin-remove has-feedback has-mandatory">
								<input name="phone" type="text" class="form-control phone-inp" placeholder="<?php the_field('text_form_phone', 'option'); ?>">
								<span class="form-control-feedback" aria-hidden="true">*</span>
							</div>
						</div>
						<div class="col-sm-2">
							<button class="btn btn-primary step-2-btn"><?php the_field('text_submit', 'option'); ?></button>
						</div>
					</div>
					<div class="hasErrorDisplayNone"><?php the_field('text_error', 'option'); ?></div>
				</div>
				<div class="form-footer step-2-form-confirm">
					<div class="checkbox-wrap">
						<input checked name="confirm" id="checkbox2" class="styled" type="checkbox">
						<label for="checkbox2">
							<?php the_field('text_calculate_10', 'option'); ?> <a href="<?php the_permalink(254); ?>" target="_blank"><?php the_field('text_calculate_11', 'option'); ?></a>
						</label>
					</div>
				</div>
				<div class="text-center">
					<button class="btn btn-primary step-3-btn"><?php the_field('text_next', 'option'); ?></button>
				</div>					
			</div>
		</form>
	</div>
</div>
<script>
	var translate = '{ "text_calculate_8": "<?php the_field('text_calculate_5', 'option'); ?>" }';
</script>