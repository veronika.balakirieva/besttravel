<?php
	$categories = get_categories(array(
		'type' => 'post',
		'orderby' => 'id',
		'order' => 'ASC',
		'taxonomy' => 'usefull',
		'hide_empty' => 0
	)); 
?>
<?php if(!empty($categories)) { ?>
<div class="content-section">
	<div class="container">
		<div class="content-section-title mb12">
			<h3 class="text-uppercase"><?php the_field('text_index_3', 'option'); ?></h3>
			<h4 class="sub-title"><?php the_field('text_index_4', 'option'); ?></h4>
			<p><?php the_field('text_index_5', 'option'); ?></p>
		</div>
		<div class="row">
			<?php foreach($categories as $category): ?>	
			<div class="col-sm-4 col-md-4">
				<?php if($category->term_id == 25) { ?>
					<a href="<?php the_permalink(216); ?>" class="best-item-col type6">
				<?php } else { ?>
					<a href="<?php echo get_category_link($category->term_id); ?>" class="best-item-col type6">
				<?php } ?>				
					<div class="img-block" style="background: url('<?php echo wp_get_attachment_image_url(get_field('image_block', $category), 'besttravel-usefull-category-block'); ?>');"></div>
					<div class="title-col">
						<strong><?php the_field('title_block_1', $category) ?></strong> 
						<span><?php the_field('title_block_2', $category) ?></span>    
					</div>
					<?php if($category->term_id != 25) { ?>
						<div class="bottom-val"><?php the_field('text_index_11', 'option'); ?> <?php echo $category->count; ?> <?php the_field('text_index_12', 'option'); ?></div>
					<?php } ?>
				</a>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
<?php } ?>