<?php 
	$wp_query = new WP_Query(array(
		'post_type' => array('insurance_companies'),
		'orderby' => 'id',
		'order' => 'ASC',
		'posts_per_page' => 5
	));

	if(!empty($wp_query)):
?>
<div class="content-section company-section margin-remove hidden-xs hidden-sm hidden-md">
	<div class="container mt_15">
		<div class="row">
		<?php if($wp_query->have_posts()): while($wp_query->have_posts()): $wp_query->the_post(); ?>
			<div class="col-sm-15">
				<div class="item-company-block">
					<div class="descr">
						<?php echo get_the_post_thumbnail($wp_query->ID, 'besttravel-price-compare', array('class' => 'company-img')); ?>
					</div>
					<a href="<?php echo get_field('link_on_site', $wp_query->ID); ?>" target="_blank" class="btn btn-primary"><?php the_field('text_insurance_companies_1', 'option'); ?></a>
				</div>
			</div>
		<?php endwhile; endif; ?>    
		</div>
	</div>
</div>	
<?php endif; wp_reset_postdata(); ?>