<!-- [* *** CONTENT START *** *] -->
<div class="content-wrap">
	<!-- section -->
	<?php while(have_posts()): the_post(); ?>
	<div class="content-section blog-content-article">
		<div class="container">
			<?php the_content();?>			
		</div>
	</div>
	<?php endwhile;?>
</div>
<!-- [* *** CONTENT AND *** *] -->