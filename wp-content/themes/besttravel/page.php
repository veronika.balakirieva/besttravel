<?php get_header(); ?>

<!-- [* *** SUB PAGE INTRO START *** *] -->
<div class="intro-top-block">
	<div class="container">
		<h1 class="title text-uppercase"><?php the_title(); ?></h1>
		<?php the_breadcrumb(); ?>
	</div>
</div>
<!-- [* *** SUB PAGE INTRO AND *** *] -->

<!-- [* *** CONTENT START *** *] -->
<div class="content-wrap">
	<!-- section -->
	<?php while(have_posts()): the_post(); ?>
	<div class="content-section blog-content-article">
		<div class="container">
			<?php the_content();?>			
		</div>
	</div>
	<?php endwhile;?>
</div>
<!-- [* *** CONTENT AND *** *] -->
	
<?php get_footer(); ?>	