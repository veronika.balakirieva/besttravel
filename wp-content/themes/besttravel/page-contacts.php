<?php get_header(); the_post(); ?>
	
<!-- [* *** SUB PAGE INTRO START *** *] -->
<div class="intro-top-block">
	<div class="container">
		<h1 class="title text-uppercase"><?php the_title(); ?></h1>
		<?php the_breadcrumb(); ?>
	</div>
</div>
<!-- [* *** SUB PAGE INTRO AND *** *] -->	

<!-- [* *** CONTENT START *** *] -->
<div class="content-wrap">
   <!-- section -->
	<div class="content-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="contact-info-col">
						<div class="img">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/cont-icon-phone.png" alt="">
						</div>
						<div class="info-box">
							<b><?php the_field('phone'); ?></b> (<?php the_field('text_contact_1', 'option'); ?>)<br>
							<b><?php the_field('phone_fax'); ?></b> (<?php the_field('text_contact_2', 'option'); ?>)
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="contact-info-col">
						<div class="img">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/cont-icon-mail.png" alt="">
						</div>
						<a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="contact-info-col">
						<div class="img">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/cont-icon-home.png" alt="">
						</div>
						<?php the_field('address'); ?><br>
						<?php the_field('address2'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- section -->
	<div class="content-section">
		<div class="container">
			<div class="content-section-title mb45">
				<h3 class="text-uppercase"><?php the_field('text_feedback', 'option'); ?></h3>
			</div>
			<?php if(l()) { ?>
				<?php echo do_shortcode('[contact-form-7 id="186" title="Контакты EL"]');?>		
			<?php } else { ?>
				<?php echo do_shortcode('[contact-form-7 id="184" title="Контакты RU"]');?>	
			<?php } ?>
		</div>
	</div>

	<!-- section -->
	<div class="content-section margin-remove">
		<div id="map"></div>
	</div>
</div>
<!-- [* *** CONTENT AND *** *] -->	
	
<script>
var map;
function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		zoom: 16,
		center: new google.maps.LatLng(<?php the_field('map_coordinates'); ?>),
		mapTypeId: 'roadmap',
		scrollwheel: false
	});

	var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';

	var icons = {
		parking: {
			icon: '<?php echo get_template_directory_uri(); ?>/assets/images/site_lot_maps.png'
		}
	};

	var features = [
		{
			position: new google.maps.LatLng(<?php the_field('map_coordinates'); ?>),
			type: 'parking'
		}
	];

	// Create markers.
	features.forEach(function(feature) {
		var marker = new google.maps.Marker({
			position: feature.position,
			icon: icons[feature.type].icon,
			map: map
		});
	});
}
</script>  
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBaZZplqSSPI6bjI4P4fnGy0cBaasHklOE&callback=initMap"></script>	
	
<?php get_footer(); ?>