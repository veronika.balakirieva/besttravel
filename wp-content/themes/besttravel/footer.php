<!-- [* *** FOOTER START *** *] -->
<footer class="footer-page">
	<div class="container">
		<div class="row">
			<div class="col-sm-9 col-md-9">
				<!-- [* menu *] -->
				<?php
					wp_nav_menu(array(
						'theme_location'  => 'menu-footer',
						'menu'            => '', 
						'container'       => 'div', 
						'container_class' => 'footer-menu', 
						'container_id'    => '',
						'menu_class'      => 'menu', 
						'menu_id'         => '',
						'echo'            => true,
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '<ul>%3$s</ul>',
						'depth'           => 0,
						'walker'          => '',
					));
				?>				
				<div class="footer-col-info">
					<div class="row">
						<div class="col-sm-6 col-md-6">
							<b><?php the_field('phone', 11); ?></b> (<?php the_field('text_contact_1', 'option'); ?>), <b><?php the_field('phone_fax', 11); ?></b> (<?php the_field('text_contact_2', 'option'); ?>)<br>
							<a href="mailto:<?php the_field('email', 11); ?>"><?php the_field('email', 11); ?></a>
						</div>
						<div class="col-sm-4 col-md-3">
							<?php the_field('address', 11); ?>
							<?php the_field('address2', 11); ?>
						</div>
						<div class="col-sm-2 col-md-2 col-md-push-1">
							<a target="_blank" class="btn btn-default" href="<?php echo get_field('footer_link_fb', 'option'); ?>" aria-label="Settings">
							  <i class="fa fa-facebook" aria-hidden="true"></i>
							</a>
							<a target="_blank" class="btn btn-default" href="<?php echo get_field('footer_link_youtube', 'option'); ?>" aria-label="Settings">
							  <i class="fa fa-youtube" aria-hidden="true"></i>
							</a>
						</div>
					</div>
				</div>
				<hr class="divider">
				<p><span class="color-yellow">*</span> <?php the_field('text_footer_1', 'option'); ?></p>
			</div>
			<div class="col-sm-2 col-md-2 col-sm-push-1">
				<!-- [* copyright col *] -->
				<div class="footer-sidebar">
					<a class="footer-logo" href="<?php echo esc_url(home_url('/')); ?>">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-site-white.png" alt="NbWeb">
					</a>
					<div class="reserved-block">
						&copy; <?php echo date('Y'); ?>.<br/> <?php the_field('text_footer_2', 'option'); ?>.<br>
                        <?php the_field('text_footer_3', 'option'); ?><br> <a target="_blank" href="http://nbweb.pro/">NbWeb</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- [* *** FOOTER AND *** *] -->

<!-- [* *** MODAL START *** *] -->
<a class="cd-button-call" data-toggle="modal" data-target=".bs-example-modal"></a>

<div class="modal fade bs-example-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center"><?php the_field('text_modal_1', 'option'); ?></h4>
			</div>
			<?php if(l()) { ?>
				<?php echo do_shortcode('[contact-form-7 id="188" title="Контакты EL"]'); ?>				
			<?php } else { ?>
				<?php echo do_shortcode('[contact-form-7 id="187" title="Контакты RU"]'); ?>				
			<?php } ?>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- [* *** MODAL AND *** *] -->

<?php wp_footer(); ?>

</body>
</html>