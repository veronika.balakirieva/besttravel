<?php
/*
Plugin Name: Insurance Request
Version: 1.0
Author: Andrey
License: GPL2
*/
defined('ABSPATH') or die('No script kiddies please!');

/**
 * Подключаем базовый класс
 */
if(class_exists('WP_List_Table') == FALSE)
{
    require_once(ABSPATH.'wp-admin/includes/class-wp-list-table.php');
}

/**
 * Отображаем таблицу лишь в том случае, если пользователь администратор
 */
if(is_admin() == TRUE)
{
    new Insurance_Request();
}
 
/**
 * Класс формирующий пункт меню и отображающий таблицу
 */
class Insurance_Request
{
    /**
     * Конструктор
     */
    public function __construct()
    {
        add_action('admin_menu', array($this, 'createMenu'));
    }
 
    /**
     * Создает пункт меню
     */
    public function createMenu()
    {
        add_menu_page('Заявки на страхование', 'Заявки на страхование', 'manage_options', 'insurance_request', array($this, 'createTable'));
    }
	
    /**
     * Отображает таблицу
     *
     */
    public function createTable()
    {
		$Table = new WP2FL_Lessons_Menu_Table_Create();
        $Table -> prepare_items();
		
        ?>
            <div class="wrap">
                <h2>Заявки на страхование</h2>
				<?php $Table -> display(); ?>
            </div>
        <?php
    }
}
 
/**
 * Класс создающий таблицу
 */
class WP2FL_Lessons_Menu_Table_Create extends WP_List_Table
{
    /**
     * Подготавливаем колонки таблицы для их отображения
     *
     */
	public function prepare_items()
	{
		//Sets
		$per_page = 15;

		/* Получаем данные для формирования таблицы */
		$data = $this -> table_data();

		/* Устанавливаем данные для пагинации */
		$this -> set_pagination_args( array(
			'total_items' => count($data),
			'per_page'    => $per_page
		));

		/* Делим массив на части для пагинации */
		$data = array_slice(
			$data,
			(($this -> get_pagenum() - 1) * $per_page),
			$per_page
		);

		/* Устанавливаем данные колонок */
		$this -> _column_headers = array(
			$this -> get_columns(), /* Получаем массив названий колонок */
			$this -> get_hidden_columns(), /* Получаем массив названий колонок которые нужно скрыть */
			$this -> get_sortable_columns() /* Получаем массив названий колонок которые можно сортировать */
		);

		/* Устанавливаем данные таблицы */
		$this -> items = $data;
	}
 
    /**
     * Название колонок таблицы
     *
     * @return array
     */
	public function get_columns()
	{
		return array(
			'id' => 'ID',
			'to_date' => 'Дата выезда',
			'from_date'	=> 'Дата приезда',
			'number_of_insured' => 'Кол-во застрахованных',
			'birthday' => 'Дата рождения',
			'baggage' => 'Багаж',	
			'firstname' => 'Имя',
			'phone' => 'Телефон',
			'date_created' => 'Дата создания'
		);
	}
	
    /**
     * Массив колонок которые нужно скрыть
     *
     * @return array
     */
	public function get_hidden_columns()
	{
		return array();
	}
 
    /**
     * Массив названий колонок по которым выполняется сортировка
     *
     * @return array
     */
	 	 
	public function get_sortable_columns()
	{
		return array(
			'to_date' => array('to_date', true),
			'from_date' => array('from_date', false),
			'baggage' => array('baggage', false),
			'date_created' => array('date_created', false)
		);
	}
 
    /**
     * Данные таблицы
     *
     * @return array
     */
	private function table_data()
	{		
		global $wpdb;

		$sql = 'SELECT * FROM ' . $wpdb->prefix . 'request ORDER BY id DESC';
		$results = $wpdb->get_results($sql, ARRAY_A);
		if (empty($results)) return false;
		
		return $results;		
	}
 
    /**
     * Возвращает содержимое колонки
     *
     * @param  array $item массив данных таблицы
     * @param  string $column_name название текущей колонки
     *
     * @return mixed
     */
    public function column_default($item, $column_name )
    {
        switch($column_name)
		{
			case 'id':
			case 'to_date':
			case 'from_date':
			case 'number_of_insured':
			case 'birthday':
			case 'baggage':
			case 'firstname':
			case 'phone':
			case 'date_created':
				return $item[$column_name];
            default:
				return print_r($item, true);
        }
    }
}