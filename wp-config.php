<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', '');

/** Имя пользователя MySQL */
define('DB_USER', '');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', '');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7T$Di>HK`!mtvzkX8Ivtz{p]m4,$TgY|c|$r:sUlqdXL#sVidi(xyPz!Ez_tB)De');
define('SECURE_AUTH_KEY',  'h,/BgTF~0W^n~o*/vbiC5&VcB=+AUK|b/L?gT0e`M<Z!xOuoss*G`%`<o#lBf3E#');
define('LOGGED_IN_KEY',    'M-xHt)RUB=RR`tLKaQKVe6vhV-|e~ycO]FB3o)%L3abNoFL;T}mUcUpOQ*Ze)%)8');
define('NONCE_KEY',        'Od%[FyX~gH 40(r=V<wpJ`]7wl.>E1J*$~B%V$)z6ocQidp`_RJHChgs>>K[/* g');
define('AUTH_SALT',        'F^n/1E/U1W*t8!$!;v*-F}TA>?(0Y[i(<.(yp!p}&O}c1hd,?`~mi*!).gRQi#24');
define('SECURE_AUTH_SALT', '1LyJ7){W~<qUp5FAD)xBm(QY%fJN,D-@zag^=X75j!Ly<-yUh|H[@(GUk7K_Y%9(');
define('LOGGED_IN_SALT',   'kwIT2Wu[p$$E3p~o5D{HNJA$$_I9{V6~wI-Yt:oO_%!^n?6l9E%@^tC%xC0>=D1r');
define('NONCE_SALT',       'O)/P*FB.B~N= >f$<o[Jx~/#O@dETz}u&cu^pxd,S;<kmlio.}_Y{]4xcJu<N8Jh');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'best_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
